# Reproducing the System.ObjectDisposedException #

1. Run app on android
2. Tap "switch" to display the chart once
3. Tap "switch" to switch away to a different page
4. Tap "switch" again to return to the original chart page
5. Rotate the App -- the app will crash with an unhandled System.ObjectDisposedException